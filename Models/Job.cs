using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace test.Models
{

    public enum Status
    {
        Ready,
        InProgress,
        Done,
        Failed
    }

    public class BloggingContext : DbContext
    {
        public DbSet<Job> Blogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseNpgsql("Server=localhost;Database=Test;Username=postgres;Password=pass;");
    }

    public class Job
    {
        public Guid JobID;
        public DateTime Created;
        public Status Status;
    }



}
